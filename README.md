# zaliczenie2 - project in angular and nodeJS

## opis pliku readme
* krok po kroku opisane skąd i jak należy pobrać repozytorium
* jak uruchomić aplikację
* jak uruchomić aplikacje w przeglądarce
* którą wersję backendu wybrać (przesłaną jako projekt 1, dostępna na http://focus-webapps.pl, inna?

## założenia działania aplikacji
* aplikacja dzwoni - wpisując numer w aplikacji frontendowej zostanie wykonane połączenie przy użyciu aplikacji backendowej
* brak numerów prywatnych w repozytorium
* komunikacja przez serwis z backendem - końcówka do wykonywania połączeń /call
* odbieranie statusów via websocket
* wyświetlanie statusów
* wykorzystanie rxjs
* obsługa błędów - {success: false}
* walidacja numeru tel.

### krok po kroku opisane skąd i jak należy pobrać repozytorium
	Wejdźmy w repozytorium zaliczenie2 i odnajdźmy przycisk Clone. Po wciśnięciu otrzymujemy link
	"git clone https://prybak2019@bitbucket.org/prybak2019/zaliczenie2.git". Wpiszmy to w lini poleceń w terminalu i wykonajmy.

### jak uruchomić aplikację
	Przejdźmy w teczkę backend poleceniem "cd backend" ale najpierw wejdźmy w teczkę zaliczenie2 jaką ściągnęliśmy w poprzednim kroku. 
	Aby wykonać polecenie node app należy uprzednio wstanowić nodeJS. Kolejnie zanim node wystaruje należy
	wykonać polecenie npm install. Potem powinniśmy otzymać wiadomość "app listening on port 3000".
	
### jak uruchomić aplikację w przeglądarce
	Należy pobrać frontIV16 z repozytorium https://bitbucket.org/prybak2019/frontiv16/src/master/. Tam podobnie 
	jak poprzednio wybrać git clone aby pobrać frontend. W pobranej teczce frontiv16 należy wstanowić frontend poleceniem
	npm install -g @angular/cli. Wykonać również polecenie npm install --save socket.io-client. Aby uruchomić frontend należy
	wykonać ng serve a potem w przegladarce wybrać localhost:4200.
	
### którą wersję backendu wybrać (przesłaną jako projekt 1, dostępna na http://focus-webapps.pl, inna?
	Backend został podany w kroku jak uruchomić aplikację.
	
### aplikacja dzwoni - wpisując numer w aplikacji frontendowej zostanie wykonane połączenie przy użyciu aplikacji backendowej
	Wpisujemy numer telefonu. Naciskamy przycisk Zadzwoń. W widget\widget.component.ts wykonuje się
	call() i jak numer jest poprawny to z call.service.ts wykonuje się placeCall przy wykorzystaniu http.post na adres
	http://localhost:3000/call z parametrami { number1: '999999999', number2: number }. W odpowiedzi dostajemy data.id co dołączamy
	jako kolejne powiadomienie callId. W backendzie w app.js widzimy że data.id to kolejny numer dla kolejnego połączenia. Widzimy że
	jest w pętli pobierany status połączenia jako interval aż połączenie się nawiąże lub odrzuci i zwracany status przez io zdarzenie
	io.emit("status", status) przy każdej zmianie. Trzeba zauważyć że currentStatus jest jedno dla wszystkich połączeń co może 
	wyzwolić błąd jak wielokrotne połączenia zostaną rozpoczęte naraz.
		W widget\widget.component.ts widzimy dalej że zapisujemy się na odbiór callId więc jak z odpowiedzi otrzymamy data.id to zapisujemy
	się na odbiór statusów. Trzeba zauważyć że jak callId przyjdzie przed naszym zapisem na powiadomienia to nic nie otrzymamy.
	Statusy są w call.service.ts w constructor jako socket.on("status", status => {	this.callStatus.next(status)	})
	czyli są wysyłane kolejne powiadomienia po odbiorze zmiany statusu.
	
### brak numerów prywatnych w repozytorium
	Wykreśliłem swoje numery z repozytorium.
	
### komunikacja przez serwis z backendem - końcówka do wykonywania połączeń /call
	Tak jak było omówione w aplikacja dzwoni, wykonuje się /call na backend.
	
### odbieranie statusów via websocket
	Tak jak było omówione w aplikacja dzwoni, statusy są wysyłane z backendu przy każdej zmianie i odbierane w frontendzie
	jako powiadomienia.
	
### wyświetlanie statusów
	Są wyświetlane po zapisie na odbiór statusów po otrzymaniu callId.
	
### wykorzystanie rxjs
	Czyli wykorzystanie powiadomień przy odbiorze statusów oraz callId.
	
### obsługa błędów - {success: false}
	Błędy są wysyłane w backendzie jako succes: false albo success: true
	
### walidacja numeru tel.
	Obsługiwana w widget\widget.component.ts jako validator = /(^[0-9]{9}$)/ w isValidNumber()
	oraz przy każdym podniesieniu palca z przycisku klawiatury w widget\widget.component.html 
	w (keyup)="isValidNumber()".